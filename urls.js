module.exports = {
  local: 'http://localhost:3000/',
  prod: 'https://elsa-prod.herokuapp.com/',
  qa: 'https://elsa-qa/herokuapp.com/',
}
