describe('React application home page', () => {
  it('Verify that the applinks says Learn React', async () => {
    await browser.url('/')
    let text = await $('.App-link').getText()
    await assert.equal(text, 'Learn React')
  })
})
